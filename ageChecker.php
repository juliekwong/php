<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://bootswatch.com/4/minty/bootstrap.min.css">
    <title>Age Checker</title>
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#">PHP Activities</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Birthstone Finder <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ageChecker.php">Age Checker</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.php">Login</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="d-flex justify-content-center align-items-center flex-column vh-100">
        <h1>Age Checker</h1>

        <!-- METHOD should always be POST -->
        <!-- ACTION = refers to the file that will handle the form -->
        <form action="controllers/process_age_checker.php" method="POST">
            <div class="form-group">
                <label for="age">What's your age?</label>

                <!-- INPUTS must have a name, para madifferentiate yung data -->
                <!-- yung "name" sa input should be the same sa "for" sa label -->
                <!-- name should be descriptive -->
                <input type="number" name="age" class="form-control">
                

            </div>
            <!-- BUTTON inside the FORM must always have type=submit -->
            <button type="submit" class="btn btn-success">Submit</button>
        </form>
    </div>
</body>
</html>