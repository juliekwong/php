<?php
    // super global variable
    // we can access this variable anywhere in our file/app regardless of scope
    // $_POST
    // var_dump($_POST["age"]);

    $age = $_POST['age'];

    if($age >= 18){
        echo "You are of legal age.";
    }else{
        echo "You are a minor.";
    }
?>