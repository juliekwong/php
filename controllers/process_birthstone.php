<!-- variables -->

<?php
$name = $_POST['name'];
$month = $_POST['month'];
$birthMonths = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$birthStones = array("Garnet", "Amethyst", "Aquamarine", "Diamond", "Emerald", "Moonstone", "Ruby", "Peridot", "Sapphire", "Tourmaline", "Topaz", "Turquoise");
$representations = array("Garnet represents confidence, energy and willpower. Those born in this month tend to be feisty and spontaneous, willing to take on any challenge that comes their way. Whether you're a lover or a fighter, this gem will keep you protected and warn of impending danger.", "Amethyst", "", "", "", "", "", "", "", "", "", "");
$imgUrl = '<img src="../assets/img/garnet-raw.jpg">';

// conditions:
// var_dump($months);
// die();

if($month === $birthMonths[0]){
    echo $birthStones[0], "<br>";
    echo $imgUrl;
    echo $representations[0];
    
    // echo '<img src="../assets/img/garnet-raw.jpg">';
    // echo file_get_contents($garnetImg);
}
// }else if($month === $birthMonths[1]){
//     echo $birthStones[1], "<br>";
//     echo $descriptions[1];
// }


?>

<link rel="stylesheet" href="https://bootswatch.com/4/minty/bootstrap.min.css">
<div class="card mb-3 justify-content-center">
    <h3 class="card-header"><?php echo $birthStones[$index]; ?></h3>
    <div class="card-body">
        <h5 class="card-title">Special title treatment</h5>
        <h6 class="card-subtitle text-muted">Support card subtitle</h6>
    </div>
    <img style="height: 200px; width: 100%; display: block;" src="" alt="Card image">
    <div class="card-body">
        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    </div>
</div>