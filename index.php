<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://bootswatch.com/4/lux/bootstrap.min.css">
    
    <title>Birthstone Finder</title>

    <style>
        body {
            background-color: #f7f0db;
background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 1600 900'%3E%3Cpolygon fill='%23c75c54' points='957 450 539 900 1396 900'/%3E%3Cpolygon fill='%2369302c' points='957 450 872.9 900 1396 900'/%3E%3Cpolygon fill='%23cf7261' points='-60 900 398 662 816 900'/%3E%3Cpolygon fill='%23824936' points='337 900 398 662 816 900'/%3E%3Cpolygon fill='%23d7886f' points='1203 546 1552 900 876 900'/%3E%3Cpolygon fill='%239b6340' points='1203 546 1552 900 1162 900'/%3E%3Cpolygon fill='%23de9f7c' points='641 695 886 900 367 900'/%3E%3Cpolygon fill='%23b37c4b' points='587 900 641 695 886 900'/%3E%3Cpolygon fill='%23e6b58a' points='1710 900 1401 632 1096 900'/%3E%3Cpolygon fill='%23cc9655' points='1710 900 1401 632 1365 900'/%3E%3Cpolygon fill='%23eecb97' points='1210 900 971 687 725 900'/%3E%3Cpolygon fill='%23e5af5f' points='943 900 1210 900 971 687'/%3E%3C/svg%3E");
background-attachment: fixed;
background-size: cover;
        }
    </style>
</head>
<body>
    <div class="container bg-secondary">

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#">PHP Activities</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Birthstone Finder <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ageChecker.php">Age Checker</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.php">Login</a>
                </li>
            </ul>
        </div>
    </nav>

            <div class="jumbotron">
                <h1>Welcome to B67's Birthstone Finder</h1>
                <hr class="my-4">
                <p class="lead">What does your birthstone says about your personality?<br>Input your name and select your birth month in the form below to find out!</p>
            </div>
        <!-- FORM -->
        <div class="row">
            <div class="col-lg-4 d-flex justify-content-center align-items-center">
                <div class="card text-white bg-primary ml-0" style="max-width: 20rem;">
                    <div class="card-body">
                        <form action="index.php" method="POST">
                            <div class="form-group">
                                <label for="name">Your Name: </label>
                                <input type="text" name="name" class="form-control">
                            </div>



                            <div class="form-group">
                                <label for="month">Birth Month:</label>
                                <select multiple="" class="form-control" id="" name="month">
                                    <option>January</option>
                                    <option>February</option>
                                    <option>March</option>
                                    <option>April</option>
                                    <option>May</option>
                                    <option>June</option>
                                    <option>July</option>
                                    <option>August</option>
                                    <option>September</option>
                                    <option>October</option>
                                    <option>November</option>
                                    <option>December</option>
                                </select>
                            </div>
                            <!-- <div class="form-group">
                                <label for="month">Birth Month:</label>
                                <input type="text" name="month" class="form-control">
                            </div> -->
                            
                            <button type="submit" class="btn btn-warning text-center">Let's check!</button>
                        </form>
                    </div>
                </div>
            </div>

            <?php
            // variables:
                $name = $_POST['name'];
                $month = $_POST['month'];
                $birthMonths = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
                $birthStones = array("Garnet", "Amethyst", "Aquamarine", "Diamond", "Emerald", "Moonstone", "Ruby", "Peridot", "Sapphire", "Tourmaline", "Topaz", "Turquoise");
                $representations = array("Are you a January baby? Your birthstone is garnet. This unique gem comes in a variety of shades, including green, orange, pink and purple. However, it's most known for its pomegranate-red hue. Garnet refers to a group of closely related stones, not just one. Almandine and pyrope are the most popular, with the latter type of garnet known for its transparency and lack of flaws.<br><br>Some gems display asterism in the form of four-rayed stars, but these are very rare. Color-changing garnet is another interesting form of the stone, changing shades when viewed in natural light. Color combinations include brown to orange and pink to red.<br><br>Garnet represents confidence, energy and willpower. Those born in this month tend to be feisty and spontaneous, willing to take on any challenge that comes their way. Whether you're a lover or a fighter, this gem will keep you protected and warn of impending danger.<br><br>We all know someone with a garnet personality — the mom who attends every PTA meeting while working full-time and keeping the house spotless. The go-getter who takes on new projects and works overtime to get ahead. The fearless student with a full course load, part-time job and noisy roommates.", 
                
                "February's birthstone is amethyst. Due to its deep purple color, early legends associated amethyst with Bacchus, the god of wine. People believed wearing the stone would prevent drunkenness. Others thought amethyst would keep the wearer clear-headed and sharp-witted during battle and business.<br><br>This gemstone representing peace and serenity has been present in religious and royal jewelry for centuries. The Pope himself wears an amethyst ring, among other jewels. It's also one of the emblems of the 12 apostles.<br><br>Those with an amethyst birthstone are known for being highly intelligent with excellent business sense. They often have a pure mind full of positive and optimistic thoughts. With a simple word or calming presence, they can set others at ease. They are also known for being great listeners, able to lend an ear to a friend in need.<br><br>Those born in February tend to be in a hurry — whether it's to get home, earn a promotion or start a family. They tend to think about the big picture, rather than break life down into its separate moments.", 
                
                "If you were born in March, your birthstone is aquamarine. Some ancient cultures believed this sea-colored stone could protect sailors and guarantee a safe voyage. During the Middle Ages, many thought wearing aquamarine would keep them safe from poisoning. Roman tradition stated you could carve a frog into the gemstone to reconcile differences between enemies.<br><br>Most stones range in color from green to blue, with dark blue stones the rarest. Like emeralds, aquamarine belongs to a variety of mineral called beryl. The six-sided crystals, which grow all over the world, can extend up to a foot long.<br><br>Those with an aquamarine birthstone can invoke the tranquility of the sea. Like those born in the previous month, March babies can summon a sense of calm in the people around them. They're known as great communicators, deeply valued for their compassion and honesty.<br><br>Aquamarine personalities make for great mediators, adept at settling disputes in the fairest way possible. They weigh both sides of an argument and consider all parties involved. Even under pressure, they're able to think clearly, express their thoughts and make decisions.", 
                
                "If you were born in April, your birthstone is the illustrious diamond. These gems form under immense pressure, about 100 miles below the Earth's surface. Then, deep volcanic eruptions eject them violently upward, at around 20 to 30 miles per hour. Here, humans eventually uncover them.<br><br>With such an eventful start, it's no wonder diamond personalities are known for their stubbornness and determination. Nothing can hold them back. Diamond personalities are incredibly hard to break, and that strength makes them courageous. They're not afraid to tackle any problem that comes their way.<br><br>Someone born in April is a person you can depend on. While fearless and stubborn, they're also fiercely loyal, willing to stand up for friends and family in times of need. A diamond is a grandma who takes in stray family members without a home and volunteers at the soup kitchen. Or an overseas volunteer building schools for underprivileged children.<br><br>You might think a diamond personality would be cocky, but those born in April are known for their innocence. They often possess a strong character and strict moral code, valuing truth and trust above all else. Due to their sparkly nature, they also have a fondness for all things luxury.", 
                
                "Those born in May bear an emerald birthstone, a favorite of Cleopatra's. Legend states the gem was one of four stones given to King Solomon by God, giving him power over all creation. When placed under the tongue, it supposedly provides the ability to see the future. Some also believed emerald could cure diseases like cholera and malaria, as well as protect against evil spells.<br><br>Emerald's luxurious green color reflects new growth. Those born in this month tend to be loving and compassionate. They have a knack for helping people see past their differences and find common ground. Often, friends seek you out for advice on work, family issues or relationships.<br><br>The gemstone has links to love and fertility. Those born in May tend to be deep-thinking romantics, seeking out flirtatious encounters and lifelong soulmates. Once you are friends with a May personality, prepare to have them in your life forever. They seek truth over falseness and value honest relationships.<br><br>Because of their perceptive abilities, emerald personalities tend to be introverted. They can get tired after spending time with large groups, aware of everyone else's emotions. A little TLC, like a bubble bath or a good book, is the perfect recharge solution.", 
                
                "Those born in June have three birthstones to choose from — pearl, alexandrite and moonstone. Choose a gem that resonates most with you and your personality or wear a combination of all three. June babies, born in early summer, are known for their enthusiasm and sense of clarity.<br><br>During the Renaissance, pearls were a symbol of nobility and prestige, worn mainly by royals. Mythology claimed pearls were the teardrops of mermaids, angels or pieces of the moon that fell into the sea. Those with a pearl personality have a reputation for being spontaneous and adept at navigating new situations. They often love to travel, even if it means leaving their comfort zone.<br><br>You might hear people call alexandrite “emerald by day, ruby by night” due to its unique color-changing abilities. Those with this birthstone tend to have a keen eye, keeping watch on everything and everyone around them. If an alexandrite personality spots a problem, they're already working on a solution.<br><br>Roman historian Pliny the Elder gave moonstone its name, claiming it shifted appearance with the moon's phases. The gem comes in a variety of shades, from yellow, peach and pink to green and gray. Those born in June often have keen instincts and should listen to their intuition. They always seem to make decisions at the most opportune moments.", 
                
                "Were you born in July? Your birthstone is the ruby, gem of kings. The nobility wore it to symbolize their power and wealth. People once thought rubies, when worn over the heart on the left side of the body, would protect the wearer and their home from threats.<br><br>The ancient Burmese believed in the ruby's protective properties as well but had different ideas about how it worked. Instead of wearing the gem, they thought they needed to insert it into their flesh. After, the precious stone would protect from spear, sword and gun wounds — perfect for battle.<br><br>It's no surprise those with a ruby personality are often showy and prone to drama. They love to sing and take part in theater performances. Their self-confidence makes them excellent leaders, even at a young age.<br><br>Those born in July are also known for their love and passion — in all aspects of life, not just relationships. They radiate energy and dazzle everyone around them. Instead of sitting around waiting for the adventure, those born in this month are prone to action. They want to be at the front of the line leading the crowd.", 
                
                "Coming in at the tail end of summer are August babies. If you were born in this month, your birthstone is peridot. With a deep green color, the gem resembles emerald. For centuries, people thought the Three Holy Kings Shrine at Cologne Cathedral in Germany was decorated with 200 carats of emeralds. However, the beautiful stones turned out to be peridot.<br><br>This gem forms deep below the Earth's mantle — with up to 95% of the global supply found in Arizona — before being propelled to the surface by volcanoes. It's also found in meteorites. Ancient Egyptians believed peridot would protect the wearer from terrors of the night, calling it the “gem of the sun.”<br><br>Those born in this month have a reputation for being incredibly kind. They offer a welcoming light to anyone they see, both strangers and friends. They tend to be open with others because they are unafraid to be themselves. Others tend to trust peridot personalities instantly due to their friendly and welcoming nature.<br><br>Those born in August tend to be highly extroverted and effortlessly charming. They don't mind the spotlight or being the center of attention. While they don't seek drama, they're able to take control of a crowd and show conviction when facing a challenge.", 
                
                "September is the month of sapphire, a stone said to instill wisdom, loyalty and nobility. While traditional sapphires are blue, the gem occurs in all colors of the rainbow, except red. The stone has been popular since the Middle Ages. Greeks wore it for guidance when asking the oracle for answers, and Buddhists thought it could bring spiritual enlightenment.<br><br>Sapphire personalities tend to be calm and reserved, only opening to those closest to them. They consider everything they say carefully and don't often chime in with their opinion in big groups. When they do build a sense of trust with others, it's usually deep, long-lasting and dependable.<br><br>Sapphire birthstone bearers tend to have a quiet type of dignity signaling intelligence. They have a keen sense of calm in any situation and, as a result, can make calculated decisions in high-pressure environments.<br><br>While those born in September can think on their feet, you won't find them bragging about it. You're more likely to hear about accomplishments secondhand from a friend or family member. They're incredibly humble and would rather talk about others over themselves.", 
                
                "If you were born in October, you can lay claim to two different birthstones — opal and pink tourmaline. Tourmaline is one of the most colorful minerals on Earth, and the pink variety perfectly represents an October-born personality. It forms in the fractures of Earth's cavities during hydrothermal activity.<br><br>Those with a pink tourmaline personality tend to be restless, explorers and adventurers looking for an untouched corner of the world. While they might seem calm and composed on the outside, those born in October tend to have racing minds, always thinking about what's coming next. They're also known for their strong intuition.<br><br>Opals, on the other hand, hold the richest and deepest colors. Each gem has a kaleidoscope effect, with blazing reds, greens, blues and purples. Many cultures have described opals as supernatural, able to guard against disease and give the gift of prophecy. The stones are also a symbol of hope, truth and purity.<br><br>Opal birthstone holders are supposedly protective and faithful, loyal to those close to them. They tend to inspire positivity everywhere they go, making them both good leaders and effective mentors. They're the ones volunteering and taking part in community projects. They're concerned about the good of all, not just themselves.", 
                
                "Those born in November have a birthstone of topaz, a label that encompasses all brown, orange and yellow transparent gems before the 1900s. Some believed the stone could attract gold, wealth and esteem. During the Middle Ages, carved topaz was thought to bring special powers. A falcon, for example, would help one reach the good graces of the king.<br><br>Saint Hildegard — a German abbess, philosopher and mystic — claimed wine-soaked topaz, when gently rubbed on the eyes, cured dim vision. Others believed topaz rendered the wearer invisible. When kept under a pillow, it can even ward off nightmares.<br><br>Topaz birthstone holders have a reputation for being lucky — especially when it comes to finances. Whether it's a fun vacation, stellar career or new car, outsiders see those born in November as having it all. That's because they work hard and persevere, even when facing insurmountable odds.<br><br>Those born in November are also known for being the life of the party. They love to have a good time and, as a result, people gravitate toward them. They're often the center of their friend group or the glue that holds the family together. Friends often describe them as generous, with an incredible capacity to forgive past transgressions.", 
                
                "There are three different birthstones attributed to December — blue topaz, tanzanite and turquoise. Those born during this month are known for being wise beyond their years, often touted as “old souls.”<br><br>One birthstone of December, blue topaz, purportedly cures various ailments and attracts love. It is also supposed to protect the wearer from others' greed. Those with a blue topaz personality are known for being friendly and easy to get along with in any situation.<br><br>Tanzanite, another December birthstone, takes its name from the blue gem's only known deposit in northern Tanzania. Those with this birthstone tend to have a love for knowledge — they're always attending a new class or have a book in their hands. They're happy to start a conversation with a stranger and debate differing opinions. This type of person can discuss politics while keeping an open and accepting mind.<br><br>December-born are also turquoise birthstone holders. The mineral, ranging in color from sky blue to yellow-green, was popular among ancient Aztecs and Egyptians. King Tut's golden funeral mask was even inlaid with turquoise. Those with a turquoise personality are known to be honest and wise. They also know how to persevere. Once they have a goal, they go on to achieve it.");
                $imgUrl = array('<img src="../assets/img/garnet.jpg">','<img src="../assets/img/amethyst.jpg">','<img src="../assets/img/aquamarine.jpg">','<img src="../assets/img/diamond.jpg">','<img src="../assets/img/emerald.jpg">','<img src="../assets/img/moonstone.jpg">','<img src="../assets/img/ruby.jpg">','<img src="../assets/img/peridot.jpg">','<img src="../assets/img/sapphire.jpg">','<img src="../assets/img/tourmaline.jpg">','<img src="../assets/img/topaz.jpg">','<img src="../assets/img/turquoise.jpg">' );

            // conditions:
            // var_dump($months);
            // die();

            // if($month === $birthMonths[0]){
            //     echo $birthStones[0], "<br>";
            //     echo $imgUrl;
            //     echo $representations[0];
                
                // echo '<img src="../assets/img/garnet-raw.jpg">';
                // echo file_get_contents($garnetImg);
            // }
            // }else if($month === $birthMonths[1]){
            //     echo $birthStones[1], "<br>";
            //     echo $descriptions[1];
            // }


            ?>

        <!-- OUTPUT -->
            <div class="col-lg-8 d-flex justify-content-center align-items-center mt-3">

                
                <?php 
                if($month === $birthMonths[0]){ ?>
                <div class="card border-primary m-5">
                    <div class="card-header p-2">
                        <h5 class="pt-3 pl-3">Hello <span class="text-warning"><?php echo $name; ?></span> !</h5>
                        <h5 class="pt-1 pl-3">Your birth stone for the month of <span class="text-warning"><?php echo $month; ?></span> is:</h5>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title text-center"><?php echo $birthStones[0]; ?></h3>
                    </div>
                    <div class="text-center">
                        <img class=""><?php echo $imgUrl[0]; ?>
                    </div>
                    <div class="card-body">
                        <h5 class="card-subtitle text-muted mb-2">Birthstone Meaning:</h5>
                        <p class="card-text"><?php echo $representations[0]; ?></p>
                    </div>
                </div>
                <?php } ?>


                <?php 
                if($month === $birthMonths[1]){ ?>
                <div class="card border-primary m-5">
                    <div class="card-header p-2">
                        <h5 class="pt-3 pl-3">Hello <span class="text-warning"><?php echo $name; ?></span> !</h5>
                        <h5 class="pt-1 pl-3">Your birth stone for the month of <span class="text-warning"><?php echo $month; ?></span> is:</h5>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title text-center"><?php echo $birthStones[1]; ?></h3>
                    </div>
                    <div class="text-center">
                        <img class=""><?php echo $imgUrl[1]; ?>
                    </div>
                    <div class="card-body">
                        <h5 class="card-subtitle text-muted mb-2">Birthstone Meaning:</h5>
                        <p class="card-text"><?php echo $representations[1]; ?></p>
                    </div>
                </div>
                <?php } ?>


                <?php 
                if($month === $birthMonths[2]){ ?>
                <div class="card border-primary m-5">
                    <div class="card-header p-2">
                        <h5 class="pt-3 pl-3">Hello <span class="text-warning"><?php echo $name; ?></span> !</h5>
                        <h5 class="pt-1 pl-3">Your birth stone for the month of <span class="text-warning"><?php echo $month; ?></span> is:</h5>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title text-center"><?php echo $birthStones[2]; ?></h3>
                    </div>
                    <div class="text-center">
                        <img class=""><?php echo $imgUrl[2]; ?>
                    </div>
                    <div class="card-body">
                        <h5 class="card-subtitle text-muted mb-2">Birthstone Meaning:</h5>
                        <p class="card-text"><?php echo $representations[2]; ?></p>
                    </div>
                </div>
                <?php } ?>


                <?php 
                if($month === $birthMonths[3]){ ?>
                <div class="card border-primary m-5">
                    <div class="card-header p-2">
                        <h5 class="pt-3 pl-3">Hello <span class="text-warning"><?php echo $name; ?></span> !</h5>
                        <h5 class="pt-1 pl-3">Your birth stone for the month of <span class="text-warning"><?php echo $month; ?></span> is:</h5>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title text-center"><?php echo $birthStones[3]; ?></h3>
                    </div>
                    <div class="text-center">
                        <img class=""><?php echo $imgUrl[3]; ?>
                    </div>
                    <div class="card-body">
                        <h5 class="card-subtitle text-muted mb-2">Birthstone Meaning:</h5>
                        <p class="card-text"><?php echo $representations[3]; ?></p>
                    </div>
                </div>
                <?php } ?>


                <?php 
                if($month === $birthMonths[4]){ ?>
                <div class="card border-primary m-5">
                    <div class="card-header p-2">
                        <h5 class="pt-3 pl-3">Hello <span class="text-warning"><?php echo $name; ?></span> !</h5>
                        <h5 class="pt-1 pl-3">Your birth stone for the month of <span class="text-warning"><?php echo $month; ?></span> is:</h5>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title text-center"><?php echo $birthStones[4]; ?></h3>
                    </div>
                    <div class="text-center">
                        <img class=""><?php echo $imgUrl[4]; ?>
                    </div>
                    <div class="card-body">
                        <h5 class="card-subtitle text-muted mb-2">Birthstone Meaning:</h5>
                        <p class="card-text"><?php echo $representations[4]; ?></p>
                    </div>
                </div>
                <?php } ?>


                <?php 
                if($month === $birthMonths[5]){ ?>
                <div class="card border-primary m-5">
                    <div class="card-header p-2">
                        <h5 class="pt-3 pl-3">Hello <span class="text-warning"><?php echo $name; ?></span> !</h5>
                        <h5 class="pt-1 pl-3">Your birth stone for the month of <span class="text-warning"><?php echo $month; ?></span> is:</h5>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title text-center"><?php echo $birthStones[5]; ?></h3>
                    </div>
                    <div class="text-center">
                        <img class=""><?php echo $imgUrl[5]; ?>
                    </div>
                    <div class="card-body">
                        <h5 class="card-subtitle text-muted mb-2">Birthstone Meaning:</h5>
                        <p class="card-text"><?php echo $representations[5]; ?></p>
                    </div>
                </div>
                <?php } ?>


                <?php 
                if($month === $birthMonths[6]){ ?>
                <div class="card border-primary m-5">
                    <div class="card-header p-2">
                        <h5 class="pt-3 pl-3">Hello <span class="text-warning"><?php echo $name; ?></span> !</h5>
                        <h5 class="pt-1 pl-3">Your birth stone for the month of <span class="text-warning"><?php echo $month; ?></span> is:</h5>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title text-center"><?php echo $birthStones[6]; ?></h3>
                    </div>
                    <div class="text-center">
                        <img class=""><?php echo $imgUrl[6]; ?>
                    </div>
                    <div class="card-body">
                        <h5 class="card-subtitle text-muted mb-2">Birthstone Meaning:</h5>
                        <p class="card-text"><?php echo $representations[6]; ?></p>
                    </div>
                </div>
                <?php } ?>


                <?php 
                if($month === $birthMonths[7]){ ?>
                <div class="card border-primary m-5">
                    <div class="card-header p-2">
                        <h5 class="pt-3 pl-3">Hello <span class="text-warning"><?php echo $name; ?></span> !</h5>
                        <h5 class="pt-1 pl-3">Your birth stone for the month of <span class="text-warning"><?php echo $month; ?></span> is:</h5>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title text-center"><?php echo $birthStones[7]; ?></h3>
                    </div>
                    <div class="text-center">
                        <img class=""><?php echo $imgUrl[7]; ?>
                    </div>
                    <div class="card-body">
                        <h5 class="card-subtitle text-muted mb-2">Birthstone Meaning:</h5>
                        <p class="card-text"><?php echo $representations[7]; ?></p>
                    </div>
                </div>
                <?php } ?>


                <?php 
                if($month === $birthMonths[8]){ ?>
                <div class="card border-primary m-5">
                    <div class="card-header p-2">
                        <h5 class="pt-3 pl-3">Hello <span class="text-warning"><?php echo $name; ?></span> !</h5>
                        <h5 class="pt-1 pl-3">Your birth stone for the month of <span class="text-warning"><?php echo $month; ?></span> is:</h5>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title text-center"><?php echo $birthStones[8]; ?></h3>
                    </div>
                    <div class="text-center">
                        <img class=""><?php echo $imgUrl[8]; ?>
                    </div>
                    <div class="card-body">
                        <h5 class="card-subtitle text-muted mb-2">Birthstone Meaning:</h5>
                        <p class="card-text"><?php echo $representations[8]; ?></p>
                    </div>
                </div>
                <?php } ?>


                <?php 
                if($month === $birthMonths[9]){ ?>
                <div class="card border-primary m-5">
                    <div class="card-header p-2">
                        <h5 class="pt-3 pl-3">Hello <span class="text-warning"><?php echo $name; ?></span> !</h5>
                        <h5 class="pt-1 pl-3">Your birth stone for the month of <span class="text-warning"><?php echo $month; ?></span> is:</h5>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title text-center"><?php echo $birthStones[9]; ?></h3>
                    </div>
                    <div class="text-center">
                        <img class=""><?php echo $imgUrl[9]; ?>
                    </div>
                    <div class="card-body">
                        <h5 class="card-subtitle text-muted mb-2">Birthstone Meaning:</h5>
                        <p class="card-text"><?php echo $representations[9]; ?></p>
                    </div>
                </div>
                <?php } ?>


                <?php 
                if($month === $birthMonths[10]){ ?>
                <div class="card border-primary m-5">
                    <div class="card-header p-2">
                        <h5 class="pt-3 pl-3">Hello <span class="text-warning"><?php echo $name; ?></span> !</h5>
                        <h5 class="pt-1 pl-3">Your birth stone for the month of <span class="text-warning"><?php echo $month; ?></span> is:</h5>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title text-center"><?php echo $birthStones[10]; ?></h3>
                    </div>
                    <div class="text-center">
                        <img class=""><?php echo $imgUrl[10]; ?>
                    </div>
                    <div class="card-body">
                        <h5 class="card-subtitle text-muted mb-2">Birthstone Meaning:</h5>
                        <p class="card-text"><?php echo $representations[10]; ?></p>
                    </div>
                </div>
                <?php } ?>


                <?php 
                if($month === $birthMonths[11]){ ?>
                <div class="card border-primary m-5">
                    <div class="card-header p-2">
                        <h5 class="pt-3 pl-3">Hello <span class="text-warning"><?php echo $name; ?></span> !</h5>
                        <h5 class="pt-1 pl-3">Your birth stone for the month of <span class="text-warning"><?php echo $month; ?></span> is:</h5>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title text-center"><?php echo $birthStones[11]; ?></h3>
                    </div>
                    <div class="text-center">
                        <img class=""><?php echo $imgUrl[11]; ?>
                    </div>
                    <div class="card-body">
                        <h5 class="card-subtitle text-muted mb-2">Birthstone Meaning:</h5>
                        <p class="card-text"><?php echo $representations[11]; ?></p>
                    </div>
                </div>
                <?php } ?>
            </div> <!-- /col-lg-8 -->
        </div> <!-- /row -->
        <footer class="d-flex justify-content-center bg-light p-2 mt-5">
        <small>Sources: <a href="https://www.developgoodhabits.com/birthstone-meanings/">Descriptions</a> | <a href="https://crystalcactus.com">Photos</a> | <a href="https://www.svgbackgrounds.com/#flat-mountains">Background</a></small>
    </footer>
    </div> <!-- /container -->
    
    
</body>
</html>






<!--
    mountain
            background-color: #f7f0db;
background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 1600 900'%3E%3Cpolygon fill='%23c75c54' points='957 450 539 900 1396 900'/%3E%3Cpolygon fill='%2369302c' points='957 450 872.9 900 1396 900'/%3E%3Cpolygon fill='%23cf7261' points='-60 900 398 662 816 900'/%3E%3Cpolygon fill='%23824936' points='337 900 398 662 816 900'/%3E%3Cpolygon fill='%23d7886f' points='1203 546 1552 900 876 900'/%3E%3Cpolygon fill='%239b6340' points='1203 546 1552 900 1162 900'/%3E%3Cpolygon fill='%23de9f7c' points='641 695 886 900 367 900'/%3E%3Cpolygon fill='%23b37c4b' points='587 900 641 695 886 900'/%3E%3Cpolygon fill='%23e6b58a' points='1710 900 1401 632 1096 900'/%3E%3Cpolygon fill='%23cc9655' points='1710 900 1401 632 1365 900'/%3E%3Cpolygon fill='%23eecb97' points='1210 900 971 687 725 900'/%3E%3Cpolygon fill='%23e5af5f' points='943 900 1210 900 971 687'/%3E%3C/svg%3E");
background-attachment: fixed;
background-size: cover;



-->